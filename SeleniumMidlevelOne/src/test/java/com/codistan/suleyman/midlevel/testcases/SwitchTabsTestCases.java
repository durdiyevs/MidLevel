package com.codistan.suleyman.midlevel.testcases;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codistan.suleyman.midlevel.switchtabs.SwitchTabs;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SwitchTabsTestCases {

	public SwitchTabsTestCases() {

	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}

//	@AfterTest
//	public void closeApp() {
//		MidlevelUtility.closeApplication();
//	}

	@Test // First Example.
	public void test() {
		//MidlevelUtility.starter("https://www.amazon.com/");
		WebDriverManager.firefoxdriver().setup();
		MidlevelUtility.driver = new FirefoxDriver();
		SwitchTabs st = new SwitchTabs();
		//st.switchTabtoParent("//a[contains(text(),'CreateSpace')]");
		st.switchTabs("https://www.google.com", "https://www.amazon.com");
	}
	@Test // Second Example.
	public void test2() {
		//MidlevelUtility.starter("https://www.amazon.com/");
		WebDriverManager.firefoxdriver().setup();
		MidlevelUtility.driver = new FirefoxDriver();
		SwitchTabs st = new SwitchTabs();
		//st.switchTabtoParent("//a[contains(text(),'CreateSpace')]");
		st.switchTabs("https://www.facebook.com", "https://www.codistan.com");
	}
	@Test // Third Example.
	public void test3() {
		//MidlevelUtility.starter("https://www.amazon.com/");
		WebDriverManager.chromedriver().setup();
		MidlevelUtility.driver = new ChromeDriver();
		SwitchTabs st = new SwitchTabs();
		//st.switchTabtoParent("//a[contains(text(),'CreateSpace')]");
		st.switchTabs("https://www.nike.com", "https://www.uber.com");
	}
	@Test // Fourth Example.
	public void test4() {
		//MidlevelUtility.starter("https://www.amazon.com/");
		WebDriverManager.chromedriver().setup();
		MidlevelUtility.driver = new ChromeDriver();
		SwitchTabs st = new SwitchTabs();
		//st.switchTabtoParent("//a[contains(text(),'CreateSpace')]");
		st.switchTabs("https://www.codingbat.com", "https://www.udemy.com");
	}
}
