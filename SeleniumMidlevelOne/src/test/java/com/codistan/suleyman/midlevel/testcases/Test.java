package com.codistan.suleyman.midlevel.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Test {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.google.com");
		System.out.println(driver.getTitle());
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		
		driver.get("https://www.yahoo.com");
		System.out.println(driver.getTitle());
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "\t");
		



	}

}
