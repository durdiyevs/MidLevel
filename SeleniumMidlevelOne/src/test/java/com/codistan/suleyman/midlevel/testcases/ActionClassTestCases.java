package com.codistan.suleyman.midlevel.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.codistan.suleyman.midlevel.actionclass.ActionClass;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class ActionClassTestCases {

	
	public ActionClassTestCases() {
		
	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}
	@AfterTest
	public void closeApp() {
		
		MidlevelUtility.driver.quit();
	}
	

	@Test // First Example.
	public void action() {
		
		MidlevelUtility.starter("http://demo.guru99.com/test/simple_context_menu.html");
	ActionClass aa = new ActionClass();
	aa.doubleClick(aa.locater);
	}
	@Test // Second Example.
	public void action2() {
		
		MidlevelUtility.starter("https://www.amazon.com");
	ActionClass aa = new ActionClass();
	aa.mouseHoverMethod(aa.menu, aa.select);
	}
	@Test // Third Example.
	public void action3() {
		
		MidlevelUtility.starter("https://www.ebay.com");
	ActionClass aa = new ActionClass();
	aa.mouseHoverMethod(aa.menu2, aa.select2);
	}
	
	
}
