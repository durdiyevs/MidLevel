package com.codistan.suleyman.midlevel.testcases;


import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import org.testng.annotations.Test;
import com.codistan.suleyman.midlevel.doubleclick.DoubleClick;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;


public class DoubleClickTestCases {

	public DoubleClickTestCases() {

	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}
	@AfterTest
	public void closeApp() {
		
		MidlevelUtility.closeApplication();
	}
	

	@Test // First Example.
	public void doubleClick() {
		
		MidlevelUtility.starter("http://demo.guru99.com/test/simple_context_menu.html");
		DoubleClick dctc = new DoubleClick();
		dctc.doubleClick(dctc.locater);
		dctc.alertAccept();
	//	MidlevelUtility.closeApplication();
	

	}
    @Test // Second Example.
	public void doubleClick2() {
		
		MidlevelUtility.starter("https://artoftesting.com/sampleSiteForSelenium");
		DoubleClick dctc = new DoubleClick();
		dctc.doubleClick(dctc.locater2);
		dctc.alertAccept();
	//	MidlevelUtility.closeApplication();
	
	}
    @Test // Third Example.
   	public void doubleClick3() {
   	
   		MidlevelUtility.starter("http://demoqa.com/tooltip-and-double-click/");
   		DoubleClick dctc = new DoubleClick();
   		dctc.doubleClick(dctc.locater3);
   		dctc.alertAccept();
   		//MidlevelUtility.closeApplication();

   	}
    @Test // Fourth Example.
   	public void doubleClick4() {
   
   		MidlevelUtility.starter("https://only-testing-blog.blogspot.com/2014/09/selectable.html");
   		DoubleClick dctc = new DoubleClick();
   		dctc.doubleClick(dctc.locater);
   		dctc.alertAccept();
   		//MidlevelUtility.closeApplication();

   	}
    @Test // Fifth Example.
   	public void doubleClick5() {

   		MidlevelUtility.starter("https://qacreators.com/PracticePage1.html");
   		DoubleClick dctc = new DoubleClick();
   		dctc.doubleClick(dctc.locater5);
   		dctc.alertAccept();
   		//MidlevelUtility.closeApplication();
    }
    
    
}
