package com.codistan.suleyman.midlevel.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.codistan.suleyman.midlevel.dropdown.DropDown;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class DropDownTestCase {


	 public DropDownTestCase() {

	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}

	@AfterTest
	public void closeApp() {
		MidlevelUtility.closeApplication();
	}

	@Test // First Example.
	public void mouseHover() {
		MidlevelUtility.starter("http://newtours.demoaut.com/mercuryregister.php");
	 DropDown dd = new DropDown();
	 dd.dropDownMethod("TURKMENISTAN", dd.locater);
	}
	
	@Test // Second Example.
	public void mouseHover2() {
		MidlevelUtility.starter("https://artoftesting.com/sampleSiteForSelenium");
	 DropDown dd = new DropDown();
	 dd.dropDownMethod("Performance Testing", dd.locater2);
	}
	
	@Test // Third Example.
	public void mouseHover3() {
		MidlevelUtility.starter("https://www.expedia.com");
	 DropDown dd = new DropDown();
	 dd.dropDownMethod("First class", dd.locater3);
	}
	
	@Test // Fourth Example.
	public void mouseHover4() {
		MidlevelUtility.starter("https://www.cargurus.com");
	 DropDown dd = new DropDown();
	 dd.dropDownMethod("Alfa Romeo", dd.locater4);
	}
	


	
	
}
