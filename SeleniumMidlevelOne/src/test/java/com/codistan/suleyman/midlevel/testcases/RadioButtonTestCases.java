package com.codistan.suleyman.midlevel.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codistan.suleyman.midlevel.radiobutton.RadioButton;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class RadioButtonTestCases {

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}

	@AfterTest
	public void closeApp() {
		MidlevelUtility.closeApplication();
	}

	@Test // First Example.
	public void mouseHover() {
		MidlevelUtility.starter("https://demoqa.com/checkboxradio");
		RadioButton rd = new RadioButton();
		rd.radioButtonCheck(rd.locater);

	}

	@Test // Second Example.
	public void mouseHover2() {
		MidlevelUtility.starter("https://artoftesting.com/sampleSiteForSelenium");
		RadioButton rd = new RadioButton();
		rd.radioButtonCheck(rd.locater2);

	}

	@Test // Third Example.
	public void mouseHover3() {
		MidlevelUtility.starter("https://material.angularjs.org/latest/demo/radioButton");
		RadioButton rd = new RadioButton();
		rd.radioButtonCheck(rd.locater3);

	}

	@Test // Fourth Example.
	public void mouseHover4() {
		MidlevelUtility.starter("https://material.angularjs.org/latest/demo/radioButton");
		RadioButton rd = new RadioButton();
		rd.radioButtonCheck(rd.locater4);

	}

}
