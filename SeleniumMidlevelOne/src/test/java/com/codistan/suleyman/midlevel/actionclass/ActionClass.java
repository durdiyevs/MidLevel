package com.codistan.suleyman.midlevel.actionclass;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class ActionClass {

	public static void main(String[] args) {

	}

	public ActionClass() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}
    //Example 1
	@FindBy(xpath = "//button[text()='Double-Click Me To See Alert']")
	public WebElement locater;
	@FindBy(xpath = "//button[@id='dblClkBtn']")
	public WebElement locater2;

	
	//double click method
	public void doubleClick(WebElement locator) {
		Actions action = new Actions(MidlevelUtility.getDriver());
		action.doubleClick(locator).perform();

	}
	
	// Example 2
	@FindBy(xpath = "//*[@id=\"nav-link-accountList\"]/span[2]")
	@CacheLookup // to optimize the
	public WebElement menu;
	@FindBy(xpath = "//span[contains(text(),'Create a List')]")
	public WebElement select;
	

	// Example 3
	@FindBy(xpath = "//li[@class='hl-cat-nav__js-tab']//a[contains(text(),'Electronics')]")
	@CacheLookup // to optimize the
	public WebElement menu2;
	@FindBy(xpath = "//a[contains(text(),'iPhones')]")
	public WebElement select2;
	
	public void mouseHoverMethod( WebElement newMenu, WebElement newSelect)  {
	       Actions act = new Actions(MidlevelUtility.driver);
	  try {
		Thread.sleep(1000);
	} catch (InterruptedException e) {
	System.out.println("Program timed out before selecting! ");
		e.printStackTrace();
	}
			act.moveToElement(newMenu).click(newSelect).build().perform();
			System.out.println(MidlevelUtility.driver.getTitle());
			
		}
}