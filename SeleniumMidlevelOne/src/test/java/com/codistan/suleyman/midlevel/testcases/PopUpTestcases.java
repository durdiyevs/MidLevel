package com.codistan.suleyman.midlevel.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.codistan.suleyman.midlevel.popup.Popup;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;


public class PopUpTestcases {

	public PopUpTestcases() {

	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}
//	@AfterTest
//	public void closeApp() {
//		
//		MidlevelUtility.closeApplication();
//	}

	@Test // First Example.
	public void popUp() {

		MidlevelUtility.starter("https://www.jquery-az.com/javascript/demo.php?ex=151.0_1");
		Popup pp = new Popup();
		MidlevelUtility.click("//button[contains(text(),'Click here to trigger JS alert')]");
		pp.alertAccept();
	}
	@Test // Second Example
	public void popUp2() {

		MidlevelUtility.starter("http://demo.automationtesting.in/Alerts.html");
		Popup pp = new Popup();
		MidlevelUtility.click("//button[@class='btn btn-danger']");
		pp.alertAccept();
	}
	@Test // Second Example
	public void popUp3() {

		MidlevelUtility.starter("https://www.jquery-az.com/javascript/demo.php?ex=151.0_3");
		Popup pp = new Popup();
		MidlevelUtility.click("//button[contains(text(),'Show a fancy alert')]");
		pp.alertAccept();
	}
	
	@Test // Second Example
	public void popUp4() {

		MidlevelUtility.starter("https://www.jquery-az.com/javascript/demo.php?ex=151.0_5");
		Popup pp = new Popup();
		MidlevelUtility.click("//button[contains(text(),'Show confirm dialogue')]");
		pp.alertAccept();
	}
	
	@Test // Second Example
	public void popUp5() {

		MidlevelUtility.starter("https://www.jquery-az.com/javascript/demo.php?ex=151.0_4");
		Popup pp = new Popup();
		MidlevelUtility.click("/html/body/div[2]/table/tbody/tr[2]/td[2]/button");
		pp.alertAccept();
	}
}