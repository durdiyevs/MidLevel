package com.codistan.suleyman.midlevel.switchtabs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class SwitchTabs {

	public static void main(String[] args) {

	}

	public SwitchTabs() {
		PageFactory.initElements(MidlevelUtility.getDriver(), this);
	}


	// First Method
	public void switchToParentTab(String url, String xpath) {
		MidlevelUtility.driver.get(url);
		System.out.println(MidlevelUtility.driver.getTitle());
		WebElement locater = MidlevelUtility.driver.findElement(By.xpath(xpath));
		locater.click();
		Set<String> st = MidlevelUtility.driver.getWindowHandles();
		Iterator<String> it = st.iterator();
		String parent = it.next();
		String child = it.next();

		// switch to child
		MidlevelUtility.driver.switchTo().window(child);
		System.out.println("After Switching");
		System.out.println(MidlevelUtility.driver.getTitle());
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//
//			e.printStackTrace();
//			System.out.println(e);
//		}

		MidlevelUtility.driver.switchTo().window(parent);
		System.out.println("Switching back to Parent");
		System.out.println(MidlevelUtility.driver.getTitle());
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//
//			e.printStackTrace();
//			System.out.println(e);
//		}
		System.out.println("Returned to parent");
	}

	// Second Method
	public void switchTabs(String a, String b) {
		MidlevelUtility.driver.get(a);
		System.out.println(MidlevelUtility.driver.getTitle());
		MidlevelUtility.driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.out.println(e);
		}
		MidlevelUtility.driver.get(b);
		System.out.println(MidlevelUtility.driver.getTitle());
		MidlevelUtility.driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "\t");

	}public void swith() {
		  
		 
	}
}
