package com.codistan.suleyman.midlevel.switchframes;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class SwitchFrames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}	public SwitchFrames() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}

	@FindBy(xpath = "//button[text()='Double-Click Me To See Alert']")
	public WebElement locater;
	@FindBy(xpath = "//button[@id='dblClkBtn']")
	public WebElement locater2;



	public void switchFramesMethod(String frameName) {
		try {
			MidlevelUtility.driver.switchTo().frame(frameName);
			Thread.sleep(2000);
			System.out.println("You Switced the Frame!");
		} catch (InterruptedException e) {
	
			System.out.println("Exception happened ! " + e.getMessage());
		}
	}

}
