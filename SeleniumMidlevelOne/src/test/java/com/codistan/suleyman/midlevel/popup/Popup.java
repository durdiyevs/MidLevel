package com.codistan.suleyman.midlevel.popup;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.PageFactory;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class Popup {

	public static void main(String[] args) {
		
	}public Popup() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}public void alertAccept() {
	      MidlevelUtility.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	      Alert alert = MidlevelUtility.driver.switchTo().alert();
	    //  String alertMessage= MidlevelUtility.driver.switchTo().alert().getText();
	      System.out.println(alert.getText());	
	      alert.accept();
	
	}public void alertDismiss() {
	      MidlevelUtility.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	      Alert alert = MidlevelUtility.driver.switchTo().alert();
	    //  String alertMessage= MidlevelUtility.driver.switchTo().alert().getText();
	      System.out.println(alert.getText());	
	      alert.dismiss();
	}
    
}

