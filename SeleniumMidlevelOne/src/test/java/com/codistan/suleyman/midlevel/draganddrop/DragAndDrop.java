package com.codistan.suleyman.midlevel.draganddrop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class DragAndDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}public DragAndDrop() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}
    
	@FindBy(xpath = "//*[@id='credit2']/a")
	public WebElement a;
	@FindBy(xpath = "//*[@id='bank']/li")
	public WebElement b;
	
	@FindBy(xpath = "//*[@id='credit1']/a")
	public WebElement a2;
	@FindBy(xpath = "//*[@id='loan']/li")
	public WebElement b2;
	
	@FindBy(xpath = "//*[@id='fourth']/a")
	public WebElement a3;
	@FindBy(xpath = "//*[@id='amt7']/li")
	public WebElement b3;
	
	@FindBy(xpath = "//*[@id='fourth']/a")
	public WebElement a4;
	@FindBy(xpath = "//*[@id='amt8']/li")
	public WebElement b4;
	
	
	
	//Second Example
	@FindBy(xpath = "//a[@id='one']")
	public WebElement c;
	@FindBy(xpath = "//a[@id='two']")
	public WebElement c2;
	@FindBy(xpath = "//a[@id='three']")
	public WebElement c3;
	@FindBy(xpath = "//a[@id='four']")
	public WebElement c4;
	@FindBy(xpath = "//a[@id='five']")
	public WebElement c5;
	
	@FindBy(xpath = "//div[@id='bin']")
	public WebElement d;
	
	
	//Third Example
	@FindBy(xpath = "//span[contains(text(),'Draggable 1')]")
	public WebElement x;
	@FindBy(xpath = "//span[contains(text(),'Draggable 2')]")
	public WebElement x2;
	@FindBy(xpath = "//span[contains(text(),'Draggable 3')]")
	public WebElement x3;
	@FindBy(xpath = "//span[contains(text(),'Draggable 4')]")
	public WebElement x4;
	
	
	@FindBy(xpath = "//div[@id='mydropzone']")
	public WebElement y;
	public void dragAndDrop (WebElement one, WebElement two) {
	   	Actions act=new Actions(MidlevelUtility.driver);	
		act.dragAndDrop(one, two).build().perform();
	}
	
	
	

}
