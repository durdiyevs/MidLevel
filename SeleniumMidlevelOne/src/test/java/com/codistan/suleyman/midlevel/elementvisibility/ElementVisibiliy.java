package com.codistan.suleyman.midlevel.elementvisibility;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class ElementVisibiliy {

	public static void main(String[] args) {

	}

	public ElementVisibiliy() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}

	@FindBy(xpath = "//button[text()='Double-Click Me To See Alert']")
	public WebElement locater;

	@FindBy(xpath = "//button[@id='dblClkBtn']")
	public WebElement locater2;

	public void isSelectedMethodWithAssert(String path) {
		WebElement element = MidlevelUtility.driver.findElement(By.xpath(path));

		try {
			Assert.assertEquals(true, element.isDisplayed());

			System.out.println("Element is Displayed!");
		} catch (NoSuchElementException e) {
			System.out.println("No Such Element is Displayed!!");
		}

	}

	public void isDisplayedMethod(String path) {
		try {
		boolean element = MidlevelUtility.driver.findElement(By.xpath(path)).isDisplayed();
		if(element==true) {
		System.out.println("Element is Displayed! ");
		} else {
			System.out.println("Element is not Displayed! ");
		}
		}catch(Exception e) {
		System.out.println("Element Not Found!");
	}
	}	public void isEnabledMethod(String path) {
		try {
		boolean element = MidlevelUtility.driver.findElement(By.xpath(path)).isEnabled();
		if(element==true) {
		System.out.println("Element is Enabled! ");
		} else {
			System.out.println("Element is not Enabled! ");
		}
		}catch(Exception e) {
		System.out.println("Element Not Found!");
	}
	}
	public void isSelectedMethod(String path) {
		try {
		boolean element = MidlevelUtility.driver.findElement(By.xpath(path)).isSelected();
		if(element==true) {
		System.out.println("Element is Selected ! ");
		} else {
			System.out.println("Element is not Selected ! ");
		}
		}catch(Exception e) {
		System.out.println("Element Not Found!");
	}
	}

}
