package com.codistan.suleyman.midlevel.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.codistan.suleyman.midlevel.mousehover.MouseHover;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class MouseHoverTestcases {

	public MouseHoverTestcases() {

	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}

	@AfterTest
	public void closeApp() {
		MidlevelUtility.closeApplication();
	}

	@Test // First Example.
	public void mouseHover() {
		MidlevelUtility.starter("https://www.amazon.com/");
		MouseHover mh = new MouseHover();
		mh.mouseHoverMethod(mh.menu, mh.select);
	}
	@Test // Second Example.
	public void mouseHover2() {
		MidlevelUtility.starter("https://artoftesting.com/sampleSiteForSelenium");
		MouseHover mh = new MouseHover();
		mh.mouseHoverMethod(mh.menu2, mh.select2);
	}
	@Test // Third Example.
	public void mouseHover3() {
		MidlevelUtility.starter("https://www.ebay.com");
		MouseHover mh = new MouseHover();
		mh.mouseHoverMethod(mh.menu3, mh.select3);
	}
	
	@Test // Fourth Example.
	public void mouseHover4() {
		MidlevelUtility.starter("https://www.aldi.us/en/shop-now/ways-to-shop-aldi");
		MouseHover mh = new MouseHover();
		mh.mouseHoverMethod(mh.menu4, mh.select4);
	}
	
	@Test // Fifth Example.
	public void mouseHover5() {
		MidlevelUtility.starter("https://www.nike.com");
		MouseHover mh = new MouseHover();
		mh.mouseHoverMethod(mh.menu5, mh.select5);
	}

}
