package com.codistan.suleyman.midlevel.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codistan.suleyman.midlevel.popup.Popup;
import com.codistan.suleyman.midlevel.switchframes.SwitchFrames;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class SwitchFrameTestCases {

	public SwitchFrameTestCases() {

	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}
//
//	@AfterTest
//	public void closeApp() {
//
//		MidlevelUtility.closeApplication();
//	}

	@Test // First Example.
	public void test() {

		MidlevelUtility.starter("https://www.toolsqa.com/iframe-practice-page/");
		SwitchFrames sf = new SwitchFrames();
		sf.switchFramesMethod("IF2");

	}
	@Test // First Example.
	public void test2() {

		MidlevelUtility.starter("https://www.toolsqa.com/iframe-practice-page/");
		SwitchFrames sf = new SwitchFrames();
		sf.switchFramesMethod("IF1");

	}

}
