package com.codistan.suleyman.midlevel.screenshot;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;

import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class ScreenShot {
	public static void main(String[] args) {

	}

	public ScreenShot() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}
	public void screenShot(String url) {
		TakesScreenshot ss = (TakesScreenshot)MidlevelUtility.driver;
		File source = ss.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("./Screenshots/"+url+".png"));  //"./Screenshots/Screen.png"
		} catch (IOException e) {
			System.out.println("Something went wrong! ");
			e.printStackTrace();
		}
		System.out.println("The Screenshot is taken for "  + url);

	}

	

}
