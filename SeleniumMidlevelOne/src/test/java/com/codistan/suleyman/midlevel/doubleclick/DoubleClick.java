package com.codistan.suleyman.midlevel.doubleclick;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class DoubleClick {

	public static void main(String[] args) {

	}

	public DoubleClick() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}

	@FindBy(xpath = "//button[text()='Double-Click Me To See Alert']")
	public WebElement locater;
	@FindBy(xpath = "//button[@id='dblClkBtn']")
	public WebElement locater2;

	@FindBy(xpath = "//button[@id='doubleClickBtn']")
	public WebElement locater3;

	@FindBy(xpath = "//button[contains(text(),'Double-Click Me To See Alert')]")
	public WebElement locater4;

	@FindBy(xpath = "//button[contains(text(),'Double Click to observe alert')]")
	public WebElement locater5;

	// double click method
	public void doubleClick(WebElement locator) {
		Actions action = new Actions(MidlevelUtility.getDriver());
		action.doubleClick(locator).perform();
	}

	public void alertAccept() {
		Alert alert = MidlevelUtility.getDriver().switchTo().alert();
		System.out.println("Alert Text\n" + alert.getText());
		alert.accept();
	}
}
