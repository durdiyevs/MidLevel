package com.codistan.suleyman.midlevel.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codistan.suleyman.midlevel.elementvisibility.ElementVisibiliy;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class ElementVisibilityTestCases {

	public ElementVisibilityTestCases() {
		
	}


	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}
	@AfterTest
	public void closeApp() {
		
		MidlevelUtility.closeApplication();
	}
	

	@Test // First Example.
	public void test1() {
		
	MidlevelUtility.starter("https://www.amazon.com");
      ElementVisibiliy el = new ElementVisibiliy();
      el.isDisplayedMethod("//a[@id='nav-cart']");
	}
	
	@Test // First Example.  
	public void test2() {
		
	MidlevelUtility.starter("http://www.testdiary.com/training/selenium/selenium-test-page/");
      ElementVisibiliy el = new ElementVisibiliy();
      el.isEnabledMethod("//button[@id='demo']");
	}
	@Test // First Example.  
	public void test3() {
		
	MidlevelUtility.starter("http://www.testdiary.com/training/selenium/selenium-test-page/");
      ElementVisibiliy el = new ElementVisibiliy();
      el.isSelectedMethod("//input[@id='restapibox']");
	}
	
}
