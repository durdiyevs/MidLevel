package com.codistan.suleyman.midlevel.dropdown;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class DropDown {

	public DropDown() {
		PageFactory.initElements(MidlevelUtility.getDriver(), this);
	}
	

	//Example1
	@CacheLookup
	@FindBy(xpath = "//select[@name='country']")
	public WebElement locater;
	
	//Example2
	@CacheLookup
	@FindBy(xpath = "//select[@id='testingDropdown']")
	public WebElement locater2;
	
	//Example3
		@CacheLookup
		@FindBy(xpath = "//select[@id='package-advanced-preferred-class-hp-package']")
		public WebElement locater3;
		
		
		//Example4
		@CacheLookup
		@FindBy(xpath = "//select[@id='carPickerUsed_makerSelect']")
		public WebElement locater4;
		
		


	
	public void dropDownMethod(String visibleText, WebElement newDropDown) {
		Select slct = new Select(newDropDown);
		slct.selectByVisibleText(visibleText);

	}

}
