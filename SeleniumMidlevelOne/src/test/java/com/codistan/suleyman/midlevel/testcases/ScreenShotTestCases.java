package com.codistan.suleyman.midlevel.testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codistan.suleyman.midlevel.radiobutton.RadioButton;
import com.codistan.suleyman.midlevel.screenshot.ScreenShot;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class ScreenShotTestCases {

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}

//	@AfterTest
//	public void closeApp() {
//		MidlevelUtility.closeApplication();
//	}

	@Test // First Example.
	public void screenshot() {
		MidlevelUtility.starter("https://www.youtube.com");
		ScreenShot ss = new ScreenShot();
		ss.screenShot("Youtube");
	}	
	@Test // Second Example.
	public void screenshot2() {
		MidlevelUtility.starter("https://www.google.com");
		ScreenShot ss = new ScreenShot();
		ss.screenShot("Google");
	
	}@Test // Second Example.
	public void screenshot3() {
		MidlevelUtility.starter("https://www.ebay.com");
		ScreenShot ss = new ScreenShot();
		ss.screenShot("Ebay");
	
	}@Test // Second Example.
	public void screenshot4() {
		MidlevelUtility.starter("https://www.facebook.com");
		ScreenShot ss = new ScreenShot();
		ss.screenShot("Facebook");
	
	}@Test // Second Example.
	public void screenshot5() {
		MidlevelUtility.starter("https://www.codistan.com");
		ScreenShot ss = new ScreenShot();
		ss.screenShot("Codistan");
	
	}
}
