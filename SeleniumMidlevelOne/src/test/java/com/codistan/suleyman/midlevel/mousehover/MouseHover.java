package com.codistan.suleyman.midlevel.mousehover;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class MouseHover {

	public MouseHover() {
		PageFactory.initElements(MidlevelUtility.getDriver(), this);
	}

	// Example 1
	@FindBy(xpath = "//*[@id=\"nav-link-accountList\"]/span[2]")
	@CacheLookup // to optimize the
	public WebElement menu;
	@FindBy(xpath = "//span[contains(text(),'Create a List')]")
	public WebElement select;
	
	
	// Example 2
	@FindBy(xpath = "//nav[@id='site-navigation']//li[@id='menu-item-97']")
	@CacheLookup // to optimize the
	public WebElement menu2;
	@FindBy(xpath = "//nav[@id='site-navigation']//a[contains(text(),'What is Manual Testing?')]")
	public WebElement select2;
	
   
	// Example 3
	@FindBy(xpath = "//li[@class='hl-cat-nav__js-tab']//a[contains(text(),'Electronics')]")
	@CacheLookup // to optimize the
	public WebElement menu3;
	@FindBy(xpath = "//a[contains(text(),'iPhones')]")
	public WebElement select3;
	
	
	// Example 4
	@FindBy(xpath = "//a[contains(text(),'Be Inspired')]")
	@CacheLookup // to optimize the
	public WebElement menu4;
	@FindBy(xpath = "//a[@class='main-nav--item--link m-level-sub'][contains(text(),'Healthy Living')]")
	public WebElement select4;
	
	// Example 5
	@FindBy(xpath = "//a[@class='nav-brand fs16-nav-sm prl5-sm pt6-sm pb6-sm nav-uppercase d-sm-ib va-sm-m'][contains(text(),'WOMEN')]")
	@CacheLookup // to optimize the
	public WebElement menu5;
	@FindBy(xpath = "//div[@id='NavigationMenu-2']//a[@class='nav-menu-item'][contains(text(),'Lifestyle')]")
	public WebElement select5;
	
	public void mouseHoverMethod( WebElement newMenu, WebElement newSelect)  {
       Actions act = new Actions(MidlevelUtility.driver);
  try {
	Thread.sleep(1000);
} catch (InterruptedException e) {
System.out.println("Program timed out before selecting! ");
	e.printStackTrace();
}
		act.moveToElement(newMenu).click(newSelect).build().perform();
		System.out.println(MidlevelUtility.driver.getTitle());
		
	}

}
