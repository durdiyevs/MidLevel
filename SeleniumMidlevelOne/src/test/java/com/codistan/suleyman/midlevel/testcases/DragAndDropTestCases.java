package com.codistan.suleyman.midlevel.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.codistan.suleyman.midlevel.doubleclick.DoubleClick;
import com.codistan.suleyman.midlevel.draganddrop.DragAndDrop;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DragAndDropTestCases {

	public DragAndDropTestCases() {

	}

	@BeforeTest
	public void startApp() {
		MidlevelUtility.getDriver();
	}
//	@AfterTest
//	public void closeApp() {
//		
//		MidlevelUtility.closeApplication();
//	}

	@Test // First Example.
	public void doubleClick() {

		MidlevelUtility.starter("http://demo.guru99.com/test/drag_drop.html");
		DragAndDrop dd = new DragAndDrop();
		dd.dragAndDrop(dd.a, dd.b);
		dd.dragAndDrop(dd.a2, dd.b2);
		dd.dragAndDrop(dd.a3, dd.b3);
		dd.dragAndDrop(dd.a4, dd.b4);

	}
	
	@Test // Second Example.
	public void doubleClick2() {

		MidlevelUtility.starter("https://bestvpn.org/html5demos/drag");
		DragAndDrop dd = new DragAndDrop();
		dd.dragAndDrop(dd.c, dd.d);
		dd.dragAndDrop(dd.c2, dd.d);
		dd.dragAndDrop(dd.c3, dd.d);
		dd.dragAndDrop(dd.c4, dd.d);

	}
	@Test // Third Example.
	public void doubleClick3() {

		MidlevelUtility.starter("https://www.seleniumeasy.com/test/drag-and-drop-demo.html");
		DragAndDrop dd = new DragAndDrop();
		dd.dragAndDrop(dd.x, dd.y);
		dd.dragAndDrop(dd.x2, dd.y);
		dd.dragAndDrop(dd.x3, dd.y);
		dd.dragAndDrop(dd.x4, dd.y);

	}
}
