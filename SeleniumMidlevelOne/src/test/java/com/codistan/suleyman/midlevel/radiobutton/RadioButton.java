package com.codistan.suleyman.midlevel.radiobutton;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.codistan.suleyman.midlevel.utility.MidlevelUtility;

public class RadioButton {

	public static void main(String[] args) {

	}

	public RadioButton() {

		PageFactory.initElements(MidlevelUtility.getDriver(), this);

	}

	// Example 1
	@FindBy(xpath = "//label[contains(text(),'New York')]")
	public WebElement locater;

	// Example 2
	@FindBy(xpath = "//input[@id='male']")
	public WebElement locater2;

	// Example 3
	@FindBy(xpath = "//md-radio-button[@id='radio_45']")
	public WebElement locater3;

	// Example 4
	@FindBy(xpath = "//md-radio-button[@id='radio_46']")
	public WebElement locater4;

	public void radioButtonCheck(WebElement option) {
		if (option.isSelected()) {
			System.out.println("RadiButton is already On");

		} else {
			System.out.println("RadioButton is turned On");
			option.click();

		}
	}

}
